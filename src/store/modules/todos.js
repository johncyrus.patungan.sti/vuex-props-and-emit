// import axios from 'axios';

// const state = {
// 	todos: [],
// };

// const getters = {
// 	allTodos: (state) => state.todos
// };

// const actions = {
// 	async fetchTodos(store) {
// 		const response = await axios.get('https://jsonplaceholder.typicode.com/todos/?_limit=5');
// 		store.commit('setTodos', response.data);
// 	},

// 	async addTodo(store, title) {
// 		const response = await axios.post('https://jsonplaceholder.typicode.com/todos', {
// 			title: title,
// 			completed: false,
// 		});

// 		store.commit('newTodo', response.data);
// 	},

// 	async deleteTodo(store, id) {
// 		await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`);
// 		store.commit('deleteTodo', id);
// 	},

// 	async filterTodos(store, e) {
// 		const limit = parseInt(e.target.options[e.target.options.selectedIndex].innerText);
// 		const response = await axios.get(`https://jsonplaceholder.typicode.com/todos/?_limit=${limit}`);
// 		console.log(response.data);
// 		store.commit('setTodos', response.data);
// 	},

// 	async updateTodo(store, updTodo) {
// 		await axios.put(`https://jsonplaceholder.typicode.com/todos/${updTodo.id}`);

// 		store.commit('updateTodo', updTodo);
// 	},
// };

// const mutations = {
// 	setTodos: (state, todos) => (state.todos = todos),
// 	newTodo: (state, todo) => state.todos.unshift(todo),
// 	deleteTodo: (state, id) => (state.todos = state.todos.filter((todo) => todo.id !== id)),
// 	updateTodo: (state, updTodo) => {
// 		const index = state.todos.findIndex((todo) => todo.id === updTodo.id);
// 		if (index !== -1) {
// 			state.todos.splice(index, 1, updTodo);
// 		}
// 	},
// };

// export default {
// 	state,
// 	mutations,
// 	actions,
// 	getters,
// };
